
const request = require('request');

var geocode = (address, callback) => {
 
request({
    url: `https://maps.googleapis.com/maps/api/geocode/json?&address=${encodeURIComponent(address)},CA&key=AIzaSyB7nY61OKaDuigi5Ewo6p0EnQjm0I-hEE8`,
    json: true
}, (error,response,body) => {
    if(error) {
        callback('unable to connect google servers');
    } else if (body.status === 'ZERO_RESULTS'){
        callback('Unable to find address');
    } else if (body.status === 'OK') {
        callback(undefined,{
            address: body.results[0].formatted_address,
            latitiude: body.results[0].geometry.location.lat,
            longitude: body.results[0].geometry.location.lng
        })
       
    }
   
});
}

module.exports = {
    geocode
}