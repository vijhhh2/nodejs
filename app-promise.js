const yargs = require('yargs');
const axios = require('axios');

const argv = yargs.options({
    a: {
        demand: true,
        alias: 'address',
        describe: 'Ftech the weather',
        string: true
    }
}).help().alias('help','h').argv;

var geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?&address=${encodeURIComponent(argv.address)},CA&key=AIzaSyB7nY61OKaDuigi5Ewo6p0EnQjm0I-hEE8`;
axios.get(geocodeUrl).then((res)=>{
    if(res.data.status === 'ZERO_RESULTS'){
        throw new Error('Unable to find address');
    }
    var lat = res.data.results[0].geometry.location.lat;
    var lng = res.data.results[0].geometry.location.lng;
    var weatherUrl = `https://api.darksky.net/forecast/e7139c9298ed2cf58b3684e084aa1cdb/${lat},${lng}`;
    console.log(res.data.results[0].formatted_address);
    return axios.get(weatherUrl);
}).then((res)=> {
    var temperature = res.data.currently.temperature;
    var apparentTemperature = res.data.currently.apparentTemperature;
    console.log(`its like ${temperature}.Feels Like ${apparentTemperature}.`);
})
.catch((e)=>{
    // console.log(e);
    if(e.code === 'ENOTFOUND') {
        console.log('Unable to connect servers');
    } else {
        console.log(e.message);
    }
})