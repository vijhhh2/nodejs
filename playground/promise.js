var asyncAdd = (a,b) => {
    return new Promise((resolve,reject)=>{
      setTimeout(()=>{
        if(typeof a === 'number' && typeof b === 'number') {
            resolve(a+b);
        } else {
            reject('Please Type Numbers');
        }
      },1000);
    });
}

asyncAdd(1,2).then((res)=>{
    console.log(res);
    return asyncAdd(res,2);
}).then((res)=>{
    console.log(res);
}).catch((error)=>{
    console.log(error);
});




// var somePromise = new Promise((resolve,reject)=> {
//     setTimeout(()=>{
//         // resolve('Hey, It Worked');
//         reject('oops! it worked');
//     },2500);
// });

// somePromise.then((message)=> {
//     console.log(message);
// },(errorMessage) => {
//     console.log(errorMessage);
// }
// );