
const request = require('request');


var geocodeAddress = (address) => {

    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            request({
                url: `https://maps.googleapis.com/maps/api/geocode/json?&address=${encodeURIComponent(address)},CA&key=AIzaSyB7nY61OKaDuigi5Ewo6p0EnQjm0I-hEE8`,
                json: true
            }, (error,response,body) => {
                if(error) {
                    reject('unable to connect google servers');
                } else if (body.status === 'ZERO_RESULTS'){
                    reject('Unable to find address');
                } else if (body.status === 'OK') {
                    resolve({
                        address: body.results[0].formatted_address,
                        latitiude: body.results[0].geometry.location.lat,
                        longitude: body.results[0].geometry.location.lng
                    })
                }
               
            });
        },2000)
    });
}

geocodeAddress('12045').then((res)=>{
    console.log(JSON.stringify(res,undefined,2));
}, (error)=>{
    console.log(error);
})