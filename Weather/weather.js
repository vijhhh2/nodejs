const request = require('request');

var getWeather = (lat,lng,callback) => {
    request({
        url:`https://api.darksky.net/forecast/e7139c9298ed2cf58b3684e084aa1cdb/${lat},${lng}`,
        json: true
    },(error,response,body) => {
        if(!error && response.statusCode === 200) {
            callback(undefined,{
                Temperature : body.currently.temperature,
                ActualTempareture:body.currently.apparentTemperature
            });
        } else {
            callback('unable to fetch weather');
        }
        
    });
}

module.exports = {
    getWeather
}