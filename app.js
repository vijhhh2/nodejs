const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./Weather/weather');

const argv = yargs.options({
    a: {
        demand: true,
        alias: 'address',
        describe: 'Ftech the weather',
        string: true
    }
}).help().alias('help','h').argv;

geocode.geocode(argv.a, (errorMsg,results) => {
    if(errorMsg) {
        console.log(errorMsg);
    } else {
        // console.log(JSON.stringify(results,undefined,2));
        console.log(results.address);
        weather.getWeather(results.latitiude,results.longitude,(errorMsg,weatherResults) => {
            if(errorMsg) {
                console.log(errorMsg);
            } else {
                console.log(`its currently ${weatherResults.Temperature}.Feels Like ${weatherResults.ActualTempareture}`);
            }
        });
    }
});




